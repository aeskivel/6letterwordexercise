﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Models;
using ConsoleApp.Implementations;
using Newtonsoft.Json;

namespace ConsoleApp {
  public static class Program {
    static void Main(string[] args) {
      Console.WriteLine("Welcome");
      Console.WriteLine("----------------------------------------");

      // Gets the parameters
      string currentDirectory = AppDomain.CurrentDomain.BaseDirectory;

      string configFilePath = currentDirectory + "settings.json";

      var parameters = GetParameters(configFilePath);

      if(parameters.ValidateSettings()) {
        var fileHandler = new FileHandler();

        // Creates a new model for processing all the items
        var processing = new Processing<string>();

        string inputFilePath = currentDirectory + parameters.SourceFileName;

        processing.Source = fileHandler.ReadFileAsList(inputFilePath);
        processing.ExpectedResults = ListProcessor.GetExpectedResults(processing.Source, parameters);
        processing.FilteredItems = ListProcessor.GetComponentElements(processing.Source, processing.ExpectedResults, parameters);

        // The method who generates the possible combination crash because the number is too big
        processing.PossibleCombinations = ListProcessor.GetPossibleMatches(processing.ExpectedResults, processing.FilteredItems, parameters);

        // Prints all the possible combinations to the console
        processing.PossibleCombinations.ForEach(i => Console.WriteLine("{0}={1}\t", i.Item1, i.Item2));
      }



      Console.WriteLine("----------------------------------------");
      Console.WriteLine("Press any key to exit");
      Console.ReadKey();
    }

    public static Parameters GetParameters(string filePath) {
      Parameters parameters = null;
      var fileHandler = new FileHandler();

      try {
        string json = fileHandler.OpenFile(filePath);
        parameters = JsonConvert.DeserializeObject<Parameters>(json);
      }
      catch (Exception e) {
        throw e;
      }

      return parameters;
    }
  }
}
