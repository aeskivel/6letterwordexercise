﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Models;

namespace ConsoleApp.Implementations {
  public static class ListProcessor {

    public static List<string> GetExpectedResults(List<string> source, Parameters parameters) {
      if (source == null || source.Count == 0) throw new ArgumentNullException(nameof(source), "The list provided is invalid");
      if (parameters == null || !parameters.ValidateSettings()) throw new ArgumentNullException(nameof(parameters), "The parameters provided are invalid");

      var results = new List<string>();

      try {
        // filters the list according to the defined parameters
        results = ApplyFilterToList(source, parameters.WordLenghtRegex);

        if (!parameters.IncludeCharacters) {
          results = ApplyFilterToList(results, parameters.NonCharRegex);
        }

        if (!parameters.IncludeWhiteSpaces) {
          results = ApplyFilterToList(results, parameters.NonWhiteSpacesRegex);
        }

        if (!parameters.IncludeNumeric) {
          results = ApplyFilterToList(results, parameters.NonNumericRegex);
        }

        if (!parameters.IncludeSpecialCharacters) {
          // not implemented
          //myRegex = new Regex(@"^[\w\s]{1,}$");
          //results = FilterList(results, myRegex, false);
        }


      }
      catch (Exception e) {
        throw e;
      }
      return results;
    }

    /// <summary>
    /// Applies a regex style filter to the list
    /// </summary>
    /// <param name="source"></param>
    /// <param name="filter"></param>
    /// <returns></returns>
    public static List<string> ApplyFilterToList(List<string> source, string filter) {
      if (source == null || source.Count == 0) throw new ArgumentNullException(nameof(source), "The list provided is invalid");
      if (string.IsNullOrEmpty(filter) || string.IsNullOrWhiteSpace(filter)) throw new ArgumentNullException(nameof(filter), "The filter provided is invalid");
      
      var results = new List<string>();

      try {
        results = source
                .Where(x => new Regex(filter).IsMatch(x))
                .ToList();
      }
      catch (Exception e) {
        throw e;
      }

      return results;
    }

    /// <summary>
    /// Gets only the 'componets' of the results
    /// </summary>
    /// <param name="source"></param>
    /// <param name="filtered"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public static List<string> GetComponentElements(List<string> source, List<string> filtered, Parameters parameters) {
      
      // Removes the results from the list
      List<string> components = source.Where(p => !filtered.Any(p2 => p2 == p)).ToList();

      var results = new List<string>();

      // Applies the same filters than in the main list
      if (!parameters.IncludeCharacters) {
        components = ApplyFilterToList(components, parameters.NonCharRegex);
      }

      if (!parameters.IncludeWhiteSpaces) {
        components = ApplyFilterToList(components, parameters.NonWhiteSpacesRegex);
      }

      if (!parameters.IncludeNumeric) {
        components = ApplyFilterToList(components, parameters.NonNumericRegex);
      }

      foreach (string word in components) {
        if (filtered.Any(x => x.Contains(word)))
          results.Add(word);
      }

      return results;
    }

    /// <summary>
    /// Creates a list of tupples with possible matches
    /// </summary>
    /// <param name="expected"></param>
    /// <param name="parts"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public static List<Tuple<string, string>> GetPossibleMatches(List<string> expected, List<string> parts, Parameters parameters) {
      var results = new List<Tuple<string, string>>();
      var possibleCombinations = new List<List<string>>();

      foreach (var item in expected) {
        var toConcatenate = new List<string>();

        // Gets only the items parts that are used in the current word
        foreach (var i in parts) {
          if (item.Contains(i)) {
            toConcatenate.Add(i);
          }
        }

        // generates the possible combinations before filtering
        possibleCombinations = GetCombinations(toConcatenate);
      }

      // Filters each combination to get only the ones that generates the valid results
      if (possibleCombinations.Count != 0) {
        results = ValidateCombinations(expected, possibleCombinations, parameters);
      }

      return results;
    }

    /// <summary>
    /// Gets a series of list of combinations for each item in the list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <returns></returns>
    public static List<List<T>> GetCombinations<T>(List<T> list) {
      var comboCount = (int)Math.Pow(2, list.Count) - 1;
      var result = new List<List<T>>();

      // uses 'i' as bitmask to choose each combo members
      for (var i = 1; i < comboCount + 1; i++) {
        // each cobination
        result.Add(new List<T>());

        for (int j = 0; j < list.Count; j++) {
          if ((i >> j) % 2 != 0)
            result.Last().Add(list[j]);
        }
      }

      return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="expected"></param>
    /// <param name="combinations"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public static List<Tuple<string, string>> ValidateCombinations(List<string> expected, List<List<string>> combinations, Parameters parameters) {
      if (expected == null || expected.Count == 0) throw new ArgumentNullException(nameof(expected), "The argument provided is invalid");
      if (combinations == null || combinations.Count == 0) throw new ArgumentNullException(nameof(combinations), "The argument provided is invalid");
      if (parameters == null) throw new ArgumentNullException(nameof(parameters), "The argument provided is invalid");

      var results = new List<Tuple<string, string>>();

      // Concatenates every combination
      foreach (var combo in combinations) {
        // Concatenates the list to compare the result with the expected values
        string concatenated = string.Join("", combo);

        if (expected.Any(x => concatenated.Equals(x, StringComparison.InvariantCultureIgnoreCase))) {
          results.Add(new Tuple<string, string>(string.Join("+", combo), concatenated));
        }
      }

      return results;
    }

  }
}
