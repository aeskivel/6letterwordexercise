﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ConsoleApp.Interfaces;

namespace ConsoleApp.Implementations {
  public class FileHandler : IFileHandler {
    public bool CheckIfFileExist(string filePath) {
      if (string.IsNullOrEmpty(filePath) || string.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath), "The argument provided is invalid");

      return File.Exists(filePath);
    }

    /// <summary>
    /// reads a file into a string
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public string OpenFile(string filePath) {
      if (string.IsNullOrEmpty(filePath) || string.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath), "The argument provided is invalid");

      string result = "";

      if (CheckIfFileExist(filePath)) {
        try {
          using (var reader = new StreamReader(filePath)) {
            result = reader.ReadToEnd();
          }
        }
        catch {
          throw;
        }        
      }

       return result;
    }

    public List<string> ReadFileAsList(string filePath) {
      if (string.IsNullOrEmpty(filePath) || string.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath), "The argument provided is invalid");

      var result = new List<string>();

      if (CheckIfFileExist(filePath)) {
        try {
          result = File.ReadAllLines(filePath)
          .ToList();
        }
        catch {
          throw;
        }
      }

      return result;
    }
  }
}
