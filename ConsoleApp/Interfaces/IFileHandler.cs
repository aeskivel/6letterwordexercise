﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Interfaces {
  public interface IFileHandler {
    bool CheckIfFileExist(string filePath);
    string OpenFile(string filePath);
    List<string> ReadFileAsList(string filePath);
  }
}
