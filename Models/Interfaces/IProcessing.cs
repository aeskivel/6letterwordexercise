﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Interfaces {
  public interface IProcessing<T> {
    List<T> Source { get; set; }
    List<T> ExpectedResults { get; set; }
    List<T> FilteredItems { get; set; }
    List<Tuple<T, T>> PossibleCombinations { get; set; }
    List<T> ValidCombinations { get; set; }
  }
}
