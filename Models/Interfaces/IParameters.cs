﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Interfaces {
  public interface IParameters {
    // Default parameters that can be used in the application

    short MinimumWordLenght { get; set; }
    short MaximumWordLenght { get; set; }
    short MaximumNumberOfConcatenations { get; set; }
    bool IncludeItSelf { get; set; }
    bool KeepDuplicatedResults { get; set; }
    bool IncludeWhiteSpaces { get; set; }
    bool IncludeNumeric { get; set; }
    bool IncludeSpecialCharacters { get; set; }
    bool IncludeCharacters { get; set; }
    string SourceFileName { get; set; }

    string NonCharRegex { get; }
    string NonWhiteSpacesRegex { get; }
    string NonNumericRegex { get; }
    string WordLenghtRegex { get; }

    bool ValidateSettings();
  }
}
