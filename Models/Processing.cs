﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Models.Interfaces;

namespace Models {
  public class Processing<T> : IProcessing<T> {
    public List<T> Source { get; set; }
    public List<T> ExpectedResults { get; set; }
    public List<T> FilteredItems { get; set; }
    public List<Tuple<T, T>> PossibleCombinations { get; set; }
    public List<T> ValidCombinations { get; set; }
  }
}
