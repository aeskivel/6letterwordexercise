﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Models.Interfaces;

namespace Models {
  public class Parameters : IParameters {

    public short MinimumWordLenght { get; set; }
    public short MaximumWordLenght { get; set; }
    public short MaximumNumberOfConcatenations { get; set; }
    public bool IncludeItSelf { get; set; }
    public bool KeepDuplicatedResults { get; set; }
    public bool IncludeWhiteSpaces { get; set; }
    public bool IncludeNumeric { get; set; }
    public bool IncludeSpecialCharacters { get; set; }
    public bool IncludeCharacters { get; set; }
    public string SourceFileName { get; set; }

    // Default parameters to handle the regex filtering
    public string NonCharRegex => @"^[^a-zA-Z]*$";

    public string NonWhiteSpacesRegex => @"^[a-zA-Z0-9]*$";

    public string NonNumericRegex => @"\b[^\d\W]+\b";

    public string WordLenghtRegex => @"^.{" + MinimumWordLenght + "," + MaximumWordLenght + "}$";
    /// <summary>
    /// Checks that the parameters are valid
    /// </summary>
    /// <returns></returns>
    public bool ValidateSettings() {
      if (MaximumWordLenght == 0)
        return false;

      if (MinimumWordLenght > MaximumWordLenght)
        return false;

      if (!IncludeWhiteSpaces && !IncludeNumeric && !IncludeSpecialCharacters && !IncludeCharacters)
        return false;

      if (string.IsNullOrEmpty(SourceFileName) || string.IsNullOrWhiteSpace(SourceFileName))
        return false;

      return true;
    }
  }
}
